package escuela;

public class Alumno {
    
    private final String nombre;
    private final int nota1, nota2, nota3;
    private final float promedio;
    
    public Alumno(String nombre, int nota1, int nota2, int nota3, float promedio){
        this.nombre=nombre ;
        this.nota1=nota1;
        this.nota2=nota2;
        this.nota3=nota3;
        this.promedio=promedio;
    }
    
    public String getNombre(){
        return this.nombre;
    }

    public float getPromedio(){
        return this.promedio;
    }
    
    
}
