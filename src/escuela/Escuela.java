package escuela;
import java.util.Scanner;

public class Escuela {

    public static void main(String[] args) {
        String nombre;
        int n1, n2, n3;
        float promedio;
        Scanner entrada = new Scanner(System.in);
        
        Alumno[] alumnos = new Alumno[5];
        
        for (int i = 0; i <alumnos.length; i++) {          
            System.out.println("Ingre el nombre del alumno");
            nombre= entrada.nextLine();
           
            System.out.println("Ingrese la primer nota de "+ nombre);
            n1=entrada.nextInt();
            System.out.println("Ingrese la segunda nota de " + nombre);
            n2=entrada.nextInt();
            System.out.println("Ingrese la tercer nota de " + nombre);
            n3=entrada.nextInt();
            entrada.nextLine();
            promedio=(float)(n1+n2+n3)/3;
            alumnos[i]=new Alumno(nombre , n1, n2, n3, promedio);
                           
        }
        
        for (Alumno alumno : alumnos) {
            System.out.println("El promerdio de " + alumno.getNombre() +
                    " es de : " + alumno.getPromedio());
        }
        
    }
    
}
